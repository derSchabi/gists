#!/usr/local/bin/python

import paho.mqtt.client as mqtt
import json

doc_file = "/var/www/htdocs/spacestatus/heimatlicher_status.json"

def handle_foo(doc, msg):
    print("PUT THE DATA FROM THE MQTT MESAGE INTO INTO JSON")

def on_connect(client, userdata, flags, rc):
    print("Connected with code: " + str(rc))
    client.subscribe("#")

def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    doc = {}

    with open(doc_file, 'r') as f:
        doc = json.loads(f)
        f.close()
   
    handle_foo(doc, msg) 
    
    with open(doc_file, 'w') as f:
        f.write(json.dumps(doc))
        f.close()

client = mqtt.Client()
client.on_message = on_message
client.on_connect = on_connect

client.username_pw_set(username="servierer", password="")
client.connect("heimat", 1883, 60)
client.subscribe("sensors")

#client.loop_forever()

