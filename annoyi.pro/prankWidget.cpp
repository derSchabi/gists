#include "prankWidget.hpp"

#include <QDebug>
#include <QPainter>
#include <QColor>
#include <QTimer>
#include <QFontMetrics>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QApplication>
#include <cstdlib>
#include <ctime>
#include <cstdlib>
#include <iostream>

PrankWidget::PrankWidget(const QString &txt, QApplication *a, QWidget *parent)
	:QWidget(parent)
	,text(txt)
	,fontWidth(0)
	,fontHeight(0)
	,font(QFont())
	,flashTimer(new QTimer(this))
    ,answerBox(new QLineEdit(this))
    ,app(a)
{
	srand(time(NULL));

	flashTimer->setInterval(16);
	flashTimer->start();

	connect(flashTimer, SIGNAL(timeout()),
		this, SLOT(onFlashTimer()));

	font.setPointSize(20);
	answerBox->setFont(font);

	font.setPointSize(50);
	fontWidth = QFontMetrics(font).width(text);
	fontHeight = QFontMetrics(font).height();

    QVBoxLayout *layout = new QVBoxLayout;
    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addStretch();
    hlayout->addWidget(answerBox);
    hlayout->addStretch();


    layout->addStretch();
    layout->addStretch();
    layout->addLayout(hlayout);
    layout->addStretch();
    setLayout(layout);

    connect(answerBox, SIGNAL(returnPressed()),
            this, SLOT(onAnswer()));
}


void PrankWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	QFont font;
	font.setPointSize(50);

    painter.setBrush(QBrush(Qt::black));
	painter.drawRect(0, 0, size().width(), size().height());
	
	painter.setPen(QPen(QColor::fromRgb(rand()%255, rand()%255, rand()%255)));
	painter.setFont(font);
	painter.drawText((size().width()-fontWidth)/2, (size().height()+fontHeight)/2, text);
}

void PrankWidget::onFlashTimer()
{
	repaint();
}

void PrankWidget::onAnswer()
{
    if(!answerBox->text().isEmpty())
    {
        std::cout<<answerBox->text().toStdString()<<std::endl;
        app->quit();
    }
}