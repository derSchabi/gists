#pragma once

#include <QWidget>
#include <QTimer>

class QLineEdit;
class QApplication;

class PrankWidget
	: public QWidget
{
	Q_OBJECT
public:
	PrankWidget(const QString &txt, QApplication *a, QWidget *parent = 0);

private slots:
	void onFlashTimer();
    void onAnswer();

private:
	virtual void paintEvent(QPaintEvent*);

	QString text;
	qreal fontWidth;
	qreal fontHeight;
	QFont font;
	QTimer *flashTimer;
  QLineEdit *answerBox;
  QApplication *app;
};