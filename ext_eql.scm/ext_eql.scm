
(define idiv
  (lambda (a b)
    (floor (/ a b))))

(define mod
  (lambda (a b)
    (- a (* b (idiv a b)))))

(define gcD
  (lambda (a b)
    (cond
      ((zero? b) a)
      (else
        (gcD b (mod a b))))))

(define _ext_eql
  (lambda (a b x xp y yp)
    (cond
      ((zero? b) (list a xp yp))
      (else
        (_ext_eql b (mod a b) 
           (- xp (* x (idiv a b))) x
           (- yp (* y (idiv a b))) y)))))

(define ext_eql
  (lambda (a b)
    (_ext_eql a b 0 1 1 0)))

(define invers
  (lambda (x m)
    (mod (car (cdr (ext_eql x m))) m)))