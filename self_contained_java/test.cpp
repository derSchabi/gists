#include <iostream>
#include <stdlib.h>

#include "stdint.h"
#include "jni.h"
#include "stdlib.h"

#define EXPORT __attribute__ ((visibility("default"))) __attribute__ ((used))

extern "C" {

  extern const uint8_t _binary_boot_jar_start[];
  extern const uint8_t _binary_boot_jar_end[];

  EXPORT const uint8_t* bootJar(size_t* size)
  {
    *size = _binary_boot_jar_end - _binary_boot_jar_start;
    return _binary_boot_jar_start;
  }

} // extern "C"

extern "C" void __cxa_pure_virtual(void) { abort(); }

void errorCheck(JNIEnv *e) {
    if(e->ExceptionCheck()) {
        e->ExceptionDescribe();        
        exit(1);
    }
}

int main(int argc, char **args) {
    JavaVMInitArgs vmArgs;
    vmArgs.version = JNI_VERSION_1_2;
    vmArgs.nOptions = 1;
    vmArgs.ignoreUnrecognized = JNI_TRUE;

    JavaVMOption options[vmArgs.nOptions];
    vmArgs.options = options;

    options[0].optionString = const_cast<char*>("-Xbootclasspath:[lzma.bootJar]");
    JavaVM *vm;
    void *env;
    JNI_CreateJavaVM(&vm, &env, &vmArgs);

    JNIEnv *e = static_cast<JNIEnv*>(env);

    // get classes
    jclass test = e->FindClass("Test");
    errorCheck(e);
    
    jclass jstring = e->FindClass("java/lang/String");
    errorCheck(e);

    // get methodes and objects
    jmethodID jmain = e->GetStaticMethodID(test, "main", "([Ljava/lang/String;)V");
    errorCheck(e);

    jobjectArray mainArgs = e->NewObjectArray(argc-1, jstring, 0);
    for(int i = 1; i < argc; i++) {
        e->SetObjectArrayElement(mainArgs, i-1, e->NewStringUTF(args[i]));
    }
    errorCheck(e);
    
    // exec
    
    e->CallStaticVoidMethod(test, jmain, mainArgs);
    errorCheck(e);

    vm->DestroyJavaVM();
    
    return 0;
}
