#!/bin/bash

##########################################
### Config
##########################################

# backup settings
PGPPWD="<gpg_pwd>"
DATE=`date +"%Y%m%d-%H%M"`
BDIR="./OC_BACKUP_$DATE"		# had to be relativ since tar would not accept it absolut, see ODIR
ODIR="/tmp"
BFILE="oc_backup_$DATE.tar.gz"

# owncloud stuff
OC_ROOT="/var/www/nextcloud"
OC_DATA="$OC_ROOT/data"

# log
LOGFILE="$OC_DATA/User/files/stuff/log/oc_backup.log.txt"
LOGPRFX="[%Y.%m.%d_%H:%M:%S]" 		# call with date +

# backup dav
DAV="https://mydav.com/webdav"
DAV_LOGIN="User"
DAV_PASSWD='<dav_pwd>'
DAV_MNT="/mnt/backup_dav"
DAV_DIR="$DAV_MNT/oc_backup"
DAV_MAX_FILES=20				# maximal amount that should be stored on backup dav
DAV_QUOTA=`expr 100 \* 1000 \* 1000`		# quota max of mailbox.org
DAv_QUOTAWARN=`expr 20 \* 1000 \* 1000`		# warn if less then QUOTAWARN of storage is available

# local backup
LB_DEV="/dev/disk/by-uuid/<disk_id>"
LB_DIR="/mnt/stick" 			# local backup dir

# mysql backup
SQL_ADDRESS="localhost"
SQL_DB="nextcloud"
SQL_USR="nextcloud"
SQL_PWD="<sql_pwd>"
SQL_BFILE="oc-sql_$DATE.bak"

# list of directories/files to backup
backup_files() {
#  save "source_file/dir" "dest/dir"
   save "User/files" "User"
}

##############################################
### CODE
##############################################

error_quit() {
    delete_from_tmp
    umount $DAV_MNT 2>> $LOGFILE
    echo "`date +$LOGPRFX` Backup Failed." >> $LOGFILE
    exit
}

save() {
    mkdir -p $BDIR/files/$2
    cp -r $OC_DATA/$1 $BDIR/files/$2 2>> $LOGFILE || error_quit
}

delete_from_tmp() {
   rm -r $BDIR 2>> $LOGFILE
   rm "$BFILE" 2>> $LOGFILE
   rm "$BFILE.gpg" 2>> $LOGFILE
}

if [ ! -f $LOGFILE ] ; then
    touch $LOGFILE
    chown www-data:www-data $LOGFILE
fi
echo "`date +$LOGPRFX` Start backup." >> $LOGFILE
cd $ODIR 2>> $LOGFILE || error_quit
mkdir $BDIR 2>> $LOGFILE || error_quit
# create file
mysqldump --lock-tables -h $SQL_ADDRESS -u $SQL_USR -p$SQL_PWD $SQL_DB > $BDIR/$SQL_BFILE 2>> $LOGFILE
backup_files
tar -czf $BFILE "$BDIR" 2>> $LOGFILE || error_quit
echo $PGPPWD | gpg --no-tty --passphrase-fd 0 -c $BFILE 2>> $LOGFILE || error_quit

# check if backup storage is mounted
if [ 0 -eq $(ls $LB_DIR | wc -l) ]
then
    mount $LB_DEV $LB_DIR 2>> $LOGFILE
fi

# copy into local backup storage
cp "$BFILE.gpg" $LB_DIR 2>> $LOGFILE

# login and upload to webdav
echo -e "$DAV_LOGIN\n$DAV_PASSWD" | \
        /usr/sbin/mount.davfs $DAV $DAV_MNT 2>> $LOGFILE

cp "$BFILE.gpg" "$DAV_DIR" 2>> $LOGFILE
if [ ! -f "$DAV_DIR/$BFILE.gpg" ] ; then
    echo "$BFILE.gpg failed copying to webdav" >> $LOGFILE
    error_quit
fi

# check webdav quota
DAV_SIZE=`du -bs $DAV_MNT | egrep -o [0-9] | tr -d "\n"`
if [ $DAV_SIZE -gt `expr $DAV_QUOTA - $DAV_QUOTAWARN` ] ; then
    echo "Warning: Quotamax of `expr $DAV_QUOTA / \( 1000 \* 1000 \) `MB allmoust reached: `expr $DAV_SIZE / \( 1000 \* 1000 \)`MB" >> $LOGFILE
fi

# delete oldes file from mbox if a certain maximum is reached
if [ $(ls "$DAV_DIR" | wc -l) -gt $DAV_MAX_FILES ] ; then
    rm "$DAV_DIR"/$(ls "$DAV_DIR" | sort | head -n 1) 2>> $LOGFILE
fi

# logout and delete temp files
umount $DAV_MNT 2>> $LOGFILE
delete_from_tmp
echo "`date +$LOGPRFX` Finished backup. Used space: `expr $DAV_SIZE / \( 1000 \* 1000 \) `MB" >> $LOGFILE