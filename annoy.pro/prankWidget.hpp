#pragma once

#include <QWidget>
#include <QTimer>

class PrankWidget
	: public QWidget
{
	Q_OBJECT
public:
	PrankWidget(const QString &txt, QWidget *parent = 0);

private slots:
	void onFlashTimer();

private:
	virtual void paintEvent(QPaintEvent*);

	QString text;
	qreal fontWidth;
	qreal fontHeight;
	QFont font;
	QTimer *flashTimer;
};
