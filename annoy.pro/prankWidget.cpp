#include "prankWidget.hpp"

#include <QDebug>
#include <QPainter>
#include <QColor>
#include <QTimer>
#include <QFontMetrics>
#include <cstdlib>
#include <ctime>

PrankWidget::PrankWidget(const QString &txt, QWidget *parent)
	:QWidget(parent)
	,text(txt)
	,fontWidth(0)
	,fontHeight(0)
	,font(QFont())
	,flashTimer(new QTimer(this))
{
	srand(time(NULL));

	flashTimer->setInterval(16);
	flashTimer->start();

	connect(flashTimer, SIGNAL(timeout()),
		this, SLOT(onFlashTimer()));

	font.setPointSize(50);
	fontWidth = QFontMetrics(font).width(text);
	fontHeight = QFontMetrics(font).height();
	
}


void PrankWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	QFont font;
	font.setPointSize(50);

	painter.setBrush(QBrush(QColor::fromRgb(rand()%255, rand()%255, rand()%255)));
	painter.drawRect(0, 0, size().width(), size().height());
	
	painter.setPen(QPen(QColor::fromRgb(rand()%255, rand()%255, rand()%255)));
	painter.setFont(font);
	painter.drawText((size().width()-fontWidth)/2, (size().height()+fontHeight)/2, text);
}

void PrankWidget::onFlashTimer()
{
	repaint();
}
