#include <QApplication>

#include "prankWidget.hpp"


// This app can be used to annoy other people over ssh
// Start it from command line like this
// $ ./annoy <some text>
// and <some text> will flicker on the screen.
// Your victim will not be able to quit it.

// compile it by running
// $ qmake-qt5 annoy.pro 
// $ make

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	QApplication::setOverrideCursor(Qt::BlankCursor);

	QString text;
	
	for(int i = 1; i < argc; i++)
		text += " " + QString(argv[i]);

	PrankWidget prankWidget(text);
	prankWidget.setWindowState(Qt::WindowFullScreen);
	prankWidget.show();

	app.exec();
}
