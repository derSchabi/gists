#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <algorithm>
#include <array>

#define WIDTH 640.0
#define HEIGHT 480.0

#define ALEN 100000000
std::array<u_int32_t, ALEN> a;

int comp (const void* p1, const void* p2)
{
   if(*(unsigned*)p1 < *(unsigned*)p2) return -1;
   else return *(unsigned*) p1 > *(unsigned*)p2;
}

int quit = 0;

void* sort_thread(void *args)
{
    time_t t;
    srand((u_int32_t) time(&t));

    clock_t start, stop, diff;

    int i;
    for(i = 0;i < ALEN; i++)
    {
        a[i] = (u_int32_t) rand() << 1 | rand() % 1;
    }

    start = clock();
    
    //std::sort(a.begin(), a.end());
    std::stable_sort(a.begin(), a.end());
    stop = clock();

    diff = stop - start;
    double latency = ((double) diff / CLOCKS_PER_SEC);
    printf("Time it took to sort: %f\n", latency);
    printf("%d\n", a[ALEN-1]);
    quit = 1;
    return 0;
}

void draw_bole(SDL_Surface *surface, int x, int y)
{
    struct SDL_Rect rect;
    rect.x = x;
    rect.y = 0;
    rect.h = y;
    rect.w = 1;
    SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF));
}

int main(int argc, char** argv)
{
    pthread_t thread;

    SDL_Window* window = NULL;
    SDL_Surface* surface = NULL;
    SDL_Event event;
    SDL_Init(SDL_INIT_EVERYTHING);
    window = SDL_CreateWindow("hallo welt", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
    surface = SDL_GetWindowSurface(window);

    pthread_create(&thread, NULL, sort_thread, NULL);

    SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));
   
    while(!quit)
    {
        SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));
        int x;
        for(x = 0; x < WIDTH; x++)
        {
            u_int32_t pole = a[(int)(x*(ALEN/WIDTH))];
            draw_bole(surface, x, pole * (HEIGHT/UINT32_MAX));
        }
        SDL_UpdateWindowSurface(window);
        SDL_Delay(16);
    }
    quit = 1;
   
    pthread_join(thread, NULL);
    return 0;
}
