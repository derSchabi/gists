extern crate random_fast_rng;
extern crate sdl2;
//extern crate quicksort;
extern crate dmsort;

use std::{
    sync::atomic::AtomicBool,
    sync::atomic::Ordering,
    fmt::Display,
    sync::Arc,
    thread::sleep,
    time::{
        Duration,
        Instant
    },
    thread
};
use random_fast_rng::{FastRng, Random};
use sdl2::{
    pixels::Color,
    render::WindowCanvas,
    rect::Rect
};


const ALEN: usize = 100_000_000;
const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

static mut A:[u32; ALEN] = [0; ALEN];

fn init_a() {
    let mut rng = FastRng::new();

    for i in 0..ALEN {
        let num = rng.get_u32();
        unsafe {
            A[i] = num;
        }
    }

}

fn draw_pole(canvas:&mut WindowCanvas, x:i32, y:u32) -> Result<(), String>{
    let rect = Rect::new(x, 0, 1, y);
    canvas.set_draw_color(Color::RGB(0xff, 0xff, 0xff));
    return canvas.fill_rect(rect);
}

fn error_to_string<T, E: Display>(result: Result<T, E>) -> Result<T, String> {
    match result {
        Ok(val) => Ok(val),
        Err(error) => Err(error.to_string())
    }
}

fn render(running: &AtomicBool) -> Result<(), String> {
    let uint_max = -1i32 as u32;
    let pole_ratio = HEIGHT as f32/uint_max as f32;
    let sdl = sdl2::init()?;
    let video_subsys = sdl.video()?;
    let window = error_to_string(
        video_subsys.window("Rust sort inspect", WIDTH, HEIGHT).build())?;

    let mut canvas = error_to_string(window.into_canvas().build())?;
    while running.load(Ordering::Relaxed) {
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        for x in 0..WIDTH {
            let index = (x * (ALEN as u32 / WIDTH)) as usize;
            let pole = unsafe { A[index] };
            draw_pole(&mut canvas, x as i32, (pole as f32 * pole_ratio) as u32)?;
        }
        canvas.present();
        sleep(Duration::new(0,1_000_000_000u32 / 60));
    }
    return Ok(());
}

fn main() {
    let running = Arc::new(AtomicBool::new(true));

    let running1 = running.clone();
    thread::spawn(move || {
        println!("start randomizing");
        init_a();
        println!("start sorting");
        let runtime = Instant::now();
        unsafe {dmsort::sort(&mut A)};
        println!("Duration: {}", runtime.elapsed().as_secs());
        running1.swap(false, Ordering::Relaxed);
    });

    match render(&running.clone()) {
        Ok(_) => println!("Done"),
        Err(error_code) => println!("{}", error_code)
    };
}