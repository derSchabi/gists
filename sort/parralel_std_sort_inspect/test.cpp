#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <vector>
#include <algorithm>

#define WIDTH 640.0
#define HEIGHT 480.0
#define MAX_DEBTH 2

#define ALEN 100000000
u_int32_t a[ALEN];

int quit = 0;

struct psort_param
{
    int debth;
    u_int32_t* pos;
};

int comp (const void* p1, const void* p2)
{
   if(*(unsigned*)p1 < *(unsigned*)p2) return -1;
   else return *(unsigned*) p1 > *(unsigned*)p2;
}

void merge(u_int32_t *a, int length)
{
    u_int32_t *shadow= new u_int32_t[length];
    const int half_length = length/2;
    int il, i1, i2; 
    i1 = 0; i2 = half_length;
    for(il = 0; il < length; il++)
    {
        if((a[i1] < a[i2] && i1 < half_length)
            || i2 == length)
        {
            shadow[il] = a[i1];
            i1++;
        } else {
            shadow[il] = a[i2];
            i2++;
        }
    }
    memcpy(a, shadow, sizeof(u_int32_t) * length);
    delete[] shadow;
}

void* parralel_sort(void *d)
{
    psort_param *p = (psort_param*) d; 
    int width = ALEN / pow(2, p->debth);

    if(p->debth < MAX_DEBTH)
    {
        pthread_t t1, t2;
        struct psort_param p1, p2;
        p1.debth = p2.debth = p->debth + 1;
        p1.pos = p->pos;
        p2.pos = &(p->pos[width/2]);
        pthread_create(&t1, NULL, parralel_sort, &p1);
        parralel_sort(&p2);
        pthread_join(t1, NULL);
        merge(p->pos, width);
    } else {
        //qsort(p->pos, width, sizeof(u_int32_t), comp);
        u_int32_t *end = &(p->pos)[width-1];
        std::sort(p->pos, end);
    }
    return 0;
}

void* sort_thread(void *args)
{
    time_t t;
    struct tm loc_time;
    char tmBuff[80];
    srand((u_int32_t) time(&t));

    struct timespec start, stop, diff;

    int i;
    
    for(i = 0;i < ALEN; i++)
    {
        a[i] = (u_int32_t) rand() << 1 | rand() % 1;
    }

    clock_gettime(CLOCK_REALTIME, &start);
    struct psort_param p;
    p.debth = 0;
    p.pos = a;
    parralel_sort(&p);
    clock_gettime(CLOCK_REALTIME, &stop);

    diff.tv_nsec = stop.tv_nsec - start.tv_nsec;
    diff.tv_sec = stop.tv_sec - start.tv_sec;
    double latency = diff.tv_sec + ((double) diff.tv_nsec / 1000000000.0);
    printf("Time it took to sort: %f\n", latency);
    quit = 1;

    return 0;
}

void draw_bole(SDL_Surface *surface, int x, int y)
{
    struct SDL_Rect rect;
    rect.x = x;
    rect.y = 0;
    rect.h = y;
    rect.w = 1;
    SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF));
}

int main(int argc, char** argv)
{
    pthread_t thread;

    SDL_Window* window = NULL;
    SDL_Surface* surface = NULL;
    SDL_Event event;
    SDL_Init(SDL_INIT_EVERYTHING);
    window = SDL_CreateWindow("hallo welt", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
    surface = SDL_GetWindowSurface(window);

    pthread_create(&thread, NULL, sort_thread, NULL);

    SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));
   
    while(!quit)
    {
        SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0, 0, 0));
        int x;
        for(x = 0; x < WIDTH; x++)
        {
            u_int32_t pole = a[(int)(x*(ALEN/WIDTH))];
            draw_bole(surface, x, pole * (HEIGHT/UINT32_MAX));
        }
        SDL_UpdateWindowSurface(window);
        SDL_Delay(16);
    }
    quit = 1;
    SDL_Delay(2000);   
    pthread_join(thread, NULL);
    return 0;
}
