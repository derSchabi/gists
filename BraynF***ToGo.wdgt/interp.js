function interp()
{
    var code = document.getElementById("code");
    var output = document.getElementById("output");
    var input = document.getElementById("ref_input");
    var code_arrow;
    var values = new Array(201);
    var input_arrow = 0;
    var values_arrow;
    var code_arrow;
    var maximum_loop = 0;
    
     value_arrow = 0;       
    while(value_arrow != values.length){
        values[value_arrow] = 0;
        value_arrow++;
    } 
    ////////////////   
    if(document.getElementById("scode_arrow").value == ""){                     //prüft ob programm schon gestartet wurde   
                                                                                //wenn nicht erstelle ales nötige
        while(value_arrow != values.length){
            values[value_arrow] = 0;
            value_arrow++;
        }
        value_arrow = 100;
        code_arrow = 0;
    }else{                                                                      //wenn schon lade daten
        value_arrow = document.getElementById("svalue_arrow").value * 1;
        code_arrow = document.getElementById("scode_arrow").value * 1;
        load_value(values);
    }
    
    
    while(code_arrow != code.value.length){
        switch(code.value[code_arrow]){
            case "<":
                value_arrow--;
                break;
            case ">":
                value_arrow++;
                break;
            case "+":
                values[value_arrow]++;
                break;
            case "-":
                values[value_arrow]--;
            case "[":
                if(maximum_loop == 2000000){
                    output.value="Loop Error: endless";
                    return;
                }
                    
                if(values[value_arrow] == 0){
                    while(code.value[code_arrow] != "]"){
                        code_arrow++;
                        if(code_arrow == code.value.length + 1)
                        {
                            output.value = "Loop Error: No ']' found";
                            return;
                        }
                    }
                }
                break;
            case "]":
                if(maximum_loop == 2000000){
                    output.value = "Loop Error: endless";
                    return;
                }
                if(values[value_arrow] != 0){
                    while(code.value[code_arrow] != "["){
                        code_arrow--;
                        if(code_arrow == -1)
                        {
                            output.value = "Loop Error: No '[' found";
                            return;
                        }
                    }
                }
                maximum_loop++;
                break;
            case ".":

                output.value += String.fromCharCode(values[value_arrow]);                   //convertiere zu char
    
                break;
            case ",":

                if(input.value != "" && input_arrow != input.value.length){                 //ist inputvalue lehr, oder zeiger am ende?
                        values[value_arrow] = input.value.charCodeAt(input_arrow);          //wenn nicht dan schreibe wert in values
                        
                    input_arrow++;
                }else{                                                                      //wenn schon verlange input
                    save_value(values);
                    document.getElementById("svalue_arrow").value = value_arrow;                                                          
                    document.getElementById("scode_arrow").value = code_arrow;                
                    input.value = "";
                    document.getElementById("input").value = "";                                                        
                    return;                                                                  
                }
                break;
                
            default:
        }
        code_arrow++;
    }
    document.getElementById("scode_arrow").value = "";
    document.getElementById("svalue_arrow").value = "";
    document.getElementById("save").value = "";
    document.getElementById("input").value = "";
    document.getElementById("ref_input").value = "";
    output.value +="\n [End of Code]";
    
}

function save_value(values)
{
    var save = document.getElementById("save");
    save.value = "";
    var values_arrow = 0;
    while(values_arrow != values.length){
        save.value += values[values_arrow];
        save.value += ";";
        values_arrow++;
    }
}

function load_value(values)
{
    var save = document.getElementById("save");
    var arrow = 0;                                        //arrow wird wahlweise verwendet
    var saves_arrow = 0;                                  //arrow für save.value
    var values_arrow = 0;
    var trans_array = new Array(201);                     //trans_array speichert die werte erst mal als string
    
    arrow = 0;
    while(arrow != trans_array.length){                   //hir wird jedem ellement von transaray eine lehrer wert gegeben, damit
        trans_array[arrow] = "";                            //das array nicht voll undefindet ist
        arrow++;
    }
    
    arrow = 0;                                            //adressiert die einzelnen string elemente des transaktions_arrays
    while(saves_arrow != save.value.length){              //loads the value
        if(save.value[saves_arrow] != ";"){               //bie saves.value wird immer das einzelne zeichen adressiert
            trans_array[arrow] += save.value[saves_arrow];//bei trans_array immer ein string
        }else{
            arrow++;
        }
        saves_arrow++;
    }
    
    arrow = 0;
    while(arrow != values.length){
        values[arrow] = trans_array[arrow] * 1;
        arrow++;
    }
}