;Copyright by (Schabi) Christian Schabesberger

;build this by running:
; 1. $ nasm -f elf64 -o ack.o ack.asm
; 2. $ gcc -o ackermann ack.o

global main

extern printf

%macro cprintf 4
	push rax
	push rcx

	push rbp
	mov rbp, rsp

	mov rcx, %4
	mov rdx, %3
	mov rsi, %2
	mov rdi, %1
	mov rax, 0
	call printf
	
	leave
	
	pop rcx
	pop rax
%endmacro

section .text

ack:
	push rax	;rax is m
	push rbx	;rbx in n
			;rcx is ans

	cmp rax, 0
	jne ack_elif
	add rbx, 1
	mov rcx, rbx	
	jmp ack_end
ack_elif:
	cmp rbx, 0
	jne ack_else
	sub rax, 1
	mov rbx, 1
	call ack
	jmp ack_end
ack_else:
	sub rbx, 1
	call ack
	mov rbx, rcx
	sub rax, 1
	call ack	
ack_end:
	pop rbx
	pop rax
	ret


main:
	xor rax, rax	;rax is i
	xor rbx, rbx	;rbx is j
	xor rcx, rcx	;rcx holds the return of ack

loop1:
	xor ebx, ebx			
loop2:	
	call ack
	cprintf msg, rax, rbx, rcx

	add rbx, 1
	cmp rbx, $6
	jne loop2	

	add rax, 1
	cmp rax, $6
	jne loop1

	ret

section .data
	msg db "ackermann (%d,%d) is: %d", 10, 0