#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <strings.h>
#include <stdarg.h>
#include <sys/mman.h>


#define JIT_BUF_SIZE 256 * 4096
#define FILE_BUF_SIZE 256
#define DATA_BUF_SIZE 4096

int code_index = 0;
uint8_t jit_buf[JIT_BUF_SIZE] __attribute__((aligned(4096)));
void (*f)(void) = (void (*)()) jit_buf;
int jmp_stack[256];
int jmp_stack_index = 0;

uint8_t data_buf[DATA_BUF_SIZE] __attribute__((aligned(4096)));

void write_code(int n, ...) 
{
	va_list bytes;
	va_start(bytes, n);
	int i_start = code_index;
	for(; code_index < i_start + n; code_index++)
	{
		jit_buf[code_index] = (uint8_t) va_arg(bytes, int);
	}
	va_end(bytes);
}

void override_code(int i, int n, ...) 
{
	va_list bytes;
	va_start(bytes, n);
	int i_start = i;
	for(; i < i_start + n; i++)
	{
		jit_buf[i] = (uint8_t) va_arg(bytes, int);
	}
	va_end(bytes);
}

void gen_call(uint64_t f)
{
	int64_t d = f - ((uint64_t) &jit_buf[(code_index) + 5]);
	write_code(5, 0xe8, d & 0xff, (d >> 0x8) & 0xff, (d >> 0x10) & 0xff, (d >> 0x18) & 0xff);
}

FILE* init(int argc, char **argv)
{
	if (mprotect(jit_buf, sizeof(jit_buf), PROT_READ | PROT_WRITE | PROT_EXEC) == -1)
	{
		fprintf(stderr, "mprotect on jit_buff failed\n");
		exit(1);
	}

	for(int i = 0; i < DATA_BUF_SIZE; i++)
		data_buf[i] = 0;

	if(argc < 2)
	{
		fprintf(stderr, "You need to enter the bf file you want to execute as first parameter.\n");
		exit(2);
	}

	return fopen(argv[1], "r");
}

void gen_move_right()
{
	// add	$0x1,%rbx
	write_code(4, 0x48, 0x83, 0xc3, 0x01);
}

void gen_move_left()
{
	// add	$0xffffffffffffffff,%rbx
	write_code(4, 0x48, 0x83, 0xc3, 0xff);
}

void gen_init(uint8_t *databuffer)
{

	uint64_t d = (uint64_t) databuffer;

	// Init the buffer like a function
	// push %rbp
	// mov %rsp, %rbp
	// push %rbx
	write_code(5,
			0x55,
			0x48, 0x89, 0xe5,
			0x53);

	// Place needle at the beginning of the databuffer
	// the rbx register is used as needle
	// movabs $<databuffer>, %rbx
	write_code(10,
			0x48,
			0xbb,
			d & 0xff,
			(d >> 0x8) & 0xff,
			(d >> 0x10) & 0xff,
			(d >> 0x18) & 0xff,
			(d >> 0x20) & 0xff,
			(d >> 0x28) & 0xff,
			(d >> 0x30) & 0xff,
			(d >> 0x38) & 0xff);

}

void gen_increment()
{
	// addb   $0x01, (%rbx)
	write_code(3, 
			 0x80,
			 0x03,
			 0x01);
}

void gen_decrement()
{
	// addb   $0xff,(%rbx)
	write_code(3, 
			 0x80,
			 0x03,
			 0xff);
}

void gen_jmp_on_zero(int current_index, int index_destination)
{
	// cmpb   $0x0, (%rbx)
	override_code(current_index, 3,
			0x80,
			0x3b,
			0x00);

	current_index += 3;
	uint64_t d = (uint64_t) &jit_buf[index_destination + 9]
		- (uint64_t) &jit_buf[current_index + 6];
	//printf("current_index=%x, index_destionation=%x, d=%x\n", current_index, index_destination, d);
	// je	 <target_address>
	override_code(current_index,
			6,
			0x0f,
			0x84,
			d & 0xff,
			(d >> 0x8) & 0xff,
			(d >> 0x10) & 0xff,
			(d >> 0x18) & 0xff);
}

void gen_jmp_on_non_zero(int current_index, int index_destination)
{
	// cmpb   $0x0, (%rbx)
	override_code(current_index, 3,
			0x80,
			0x3b,
			0x00);

	current_index += 3;
	uint64_t d = (uint64_t) &jit_buf[index_destination + 9]
		- (uint64_t) &jit_buf[current_index + 6];
	//printf("current_index=%x, index_destionation=%x, d=%x\n", current_index, index_destination, d);
	// jne	<target_address>
	override_code(current_index, 6,
			0x0f,
			0x85,
			d & 0xff,
			(d >> 0x8) & 0xff,
			(d >> 0x10) & 0xff,
			(d >> 0x18) & 0xff);
}

void gen_jmp_placeholder()
{
	// 9x nop
	write_code(9, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90);
}

void gen_open_jump()
{
	// push current index to jumpstack so we can override this later
	jmp_stack[jmp_stack_index++] = code_index;
	gen_jmp_placeholder();
}

void gen_close_jump()
{
	int jmp_dest = jmp_stack[--jmp_stack_index];
	gen_jmp_on_non_zero(code_index, jmp_dest);
	gen_jmp_on_zero(jmp_dest, code_index);
	code_index += 9;
}



void gen_putchar()
{
	uint64_t d = (uint64_t) data_buf;

	// mov	(%rbx), %edi
	write_code(2,
			0x8b,
			0x3b);
	// call   <putchar@plt>
	gen_call((uint64_t) putchar);
}

void gen_getchar()
{
	uint64_t d = (uint64_t) data_buf;

	// call   <getchar@plt>
	gen_call((uint64_t) getchar);

	// mov	%al, (%rbx)
	write_code(2,
			0x88,
			0x03);
}

void gen_end()
{
	// first wee need to replace the nobs for all the jumps that end the programm
	int i;
	for(i = jmp_stack_index; i > 0; i--)
	{
		int jmp_dest = jmp_stack[i];
		gen_jmp_on_zero(jmp_dest, code_index);
		code_index += 13;
	}

	// ... then we can end the programm
	// pop %rbx
	// mov %rbp, %rsp
	// pop %rbp
	// ret
	write_code(6, 0x5b,
			0x48, 0x89, 0xec,
			0x5d,
			0xc3);
}

void compile(FILE *bfile)
{
	int i;
	char c;
	uint8_t buff[FILE_BUF_SIZE];
	gen_init(data_buf);
	while(fgets(buff, FILE_BUF_SIZE, bfile) != NULL)
		for(i = 0, c = buff[i]; c != 0; c = buff[++i])
		{
			switch(c) {
				case '+': 
					gen_increment();
					break;
				case '-':
					gen_decrement();
					break;
				case '<':
					gen_move_left();
					break;
				case '>':
					gen_move_right();
					break;
				case ',':
					gen_getchar();
					break;
				case '.':
					gen_putchar();
					break;
				case '[':
					gen_open_jump();
					break;
				case ']':
					gen_close_jump();
					break;
				default:
					break;
			}
		}
	gen_end();
}

int main(int argc, char **argv)
{
	FILE *bfile = init(argc, argv);

	compile(bfile);
	fclose(bfile);

	f();

	return 0;
}
