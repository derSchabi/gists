
; Copyright (C) Christian Schabesberger 2015 <chris.schabesberger@mailbox.org>
;
; This Programm is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This Programm is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this Programm.  If not, see <http://www.gnu.org/licenses/>.
;

%include " gtk.asm"

extern exit
global main

section .text

onHelloButtonClicked:	;----------------
	push rbx
	mov eax, 4
	mov ebx, 1
	mov ecx, hw
	mov edx, hw_len
	int 0x80
	pop rbx	
	ret	

onSayHiButtonClicked:	;-----------------
	mov al, [speach_counter]
	cmp al, 0
	jne saySecond
	gtk_label_set_text_m label, hi
	jmp sayEnd
saySecond:
	cmp al, 1
	jne sayThird
	gtk_label_set_text_m label, yesHi
	jmp sayEnd
sayThird:
	cmp al, 2
	jne sayFourth
	gtk_label_set_text_m label, ok
	jmp sayEnd
sayFourth:
	cmp al, 3
	jne sayFifth
	gtk_label_set_text_m label, weHadIt
	jmp sayEnd
sayFifth:
	cmp al, 4
	jne saySixth
	gtk_label_set_text_m label, okBy
	jmp sayEnd
saySixth:
	mov rdi, 1
	call exit
sayEnd:	
	mov al, [speach_counter]
	add al, 1
	mov [speach_counter], al
	ret

main:		;---------------------
	push rbp
	mov rbp, rsp
	mov [argc], edi
	mov [argv], rsi
;---------------------
	gtk_init_m argc, argv

	gtk_builder_new_m builder
	gtk_builder_add_from_file_m_NULL builder, ui_file

	gtk_builder_get_object_m_widget window, builder, window_name
	g_signal_connect_data_m_NULL window, destroy, gtk_main_quit

	gtk_builder_get_object_m_button helloButton, builder, helloButton_name
	g_signal_connect_data_m_NULL helloButton, clicked, onHelloButtonClicked

	gtk_builder_get_object_m_button sayHiButton, builder, sayHiButton_name
	g_signal_connect_data_m_NULL sayHiButton, clicked, onSayHiButtonClicked

	gtk_builder_get_object_m_button quitButton, builder, quitButton_name
	g_signal_connect_data_m_NULL quitButton, clicked, gtk_main_quit

	gtk_builder_get_object_m_label label, builder, label_name

	gtk_widget_show_all_m window
	call gtk_main
;---------------------
	mov eax, 0
	leave
	ret


section .data
	argc dd 0
	argv dq 0

	window dq 0
	window_name db "window", 0

	helloButton dq 0
	helloButton_name db "helloButton", 0

	sayHiButton dq 0
	sayHiButton_name db "sayHiButton", 0

	quitButton dq 0
	quitButton_name db "quitButton", 0

	label dq 0
	label_name db "label", 0

	builder dq 0
	ui_file db "test.xml", 0

	hw db "Hello World", 10, 0
	hw_len equ $ - hw

	speach_counter db 0

	hi db "Hi", 0
	yesHi db "Yea Hi", 0
	ok db "ok", 0
	weHadIt db "... we had this already.", 0
	okBy db "OK BY", 0
