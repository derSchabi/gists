
; Copyright (C) Christian Schabesberger 2015 <chris.schabesberger@mailbox.org>
;
; This Programm is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This Programm is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this Programm.  If not, see <http://www.gnu.org/licenses/>.
;

extern gtk_init
extern gtk_main
extern gtk_main_quit

extern gtk_window_new

extern gtk_widget_show_all
extern gtk_widget_get_type

extern gtk_button_new_with_label
extern gtk_button_get_type

extern gtk_label_new
extern gtk_label_get_type
extern gtk_label_set_text

extern gtk_container_add
extern gtk_container_get_type

extern gtk_builder_new
extern gtk_builder_add_from_file
extern gtk_builder_get_object

extern g_type_check_instance_cast
extern g_signal_connect_data

;events
section .data
	destroy db "destroy", 0
	clicked db "clicked", 0

%macro gtk_init_m 2
	mov rdi, %1
	mov rsi, %2
	call gtk_init	
%endmacro

%macro gtk_window_new_m_toplevel 1
	mov edi, 0
	call gtk_window_new
	mov [%1], rax
%endmacro

%macro gtk_widget_show_all_m 1
	mov rdi, [%1]
	call gtk_widget_show_all
%endmacro

%macro gtk_button_new_with_label_m 2
	mov rdi, %2
	call gtk_button_new_with_label
	mov [%1], rax
%endmacro

;you need to check your self if your given object
;is an instance of container !!!

%macro gtk_container_add_m 2
	call gtk_container_get_type
	mov rsi, rax
	mov rdi, [%1]
	call g_type_check_instance_cast
	mov rdi, rax
	mov rsi, [%2]
	call gtk_container_add
%endmacro

%macro g_signal_connect_data_m_NULL 3
	mov rdi, [%1]	;sender object
	mov rsi, %2	;signal
	mov rdx, %3	;callback
	xor r8d, r8d	;NULL
	xor r9d, r9d
	xor rcx, rcx
	call g_signal_connect_data
%endmacro

%macro gtk_builder_new_m 1
	call gtk_builder_new
	mov [%1], rax
%endmacro

%macro gtk_builder_add_from_file_m_NULL 2
	mov rdi, [%1]
	mov rsi, %2
	xor rdx, rdx
	call gtk_builder_add_from_file
%endmacro

%macro __gtk_builder_get_object_m__ 3
	mov rbx, rax
	mov rdi, [%2]
	mov rsi, %3
	call gtk_builder_get_object
	mov rsi, rbx
	mov rdi, rax
	call g_type_check_instance_cast
	mov [%1], rax
%endmacro

%macro gtk_builder_get_object_m_widget 3
	call gtk_widget_get_type
	__gtk_builder_get_object_m__ %1, %2, %3
%endmacro

%macro gtk_builder_get_object_m_button 3
	call gtk_button_get_type
	__gtk_builder_get_object_m__ %1, %2, %3
%endmacro

%macro gtk_builder_get_object_m_label 3
	call gtk_label_get_type
	__gtk_builder_get_object_m__ %1, %2, %3
%endmacro

%macro gtk_label_new_m 2
	mov rdi, %2
	call gtk_label_new
	mov [%1], rax
%endmacro

%macro gtk_label_set_text_m 2
	mov rdi, [%1]
	mov rsi, %2
	call gtk_label_set_text
%endmacro