Schbis Gists
============

These are small projects I've created over the year.
They where formally hosted at M$ Github.



[gtk.asm](gtk.asm) Gtk example in assembler

[ack.asm](ack.asm) Ackerman written in nasm assembler for linux

[annoy.pro](annoy.pro) A Qt5 programm for annoying people

[annoyi.pro](annoyi.pro) Annoy improved. Less annoying and with responce function

[db_client.c](db_client.c) Dickbut for pixelflut

[ext_eql.scm](ext_eql.scm) extended euqlid written in scheme

[micro_ftp](micro_ftp) little ftp server written in python

[newton_bot.bash](newton_bot.bash) Simple [NewtonWars](https://github.com/Draradech/NewtonWars) bot written in bash

[owncloud_mailbox_org_backup.sh](owncloud_mailbox_org_backup.sh) backup owncloud to webdav

[pixel_arduino.ino](pixel_arduino.ino) Pixelflut client for arduino

[pong.html](pong.html) pong

[self_contained_java](self_contained_java) test for writing a self contain java application

[snake.html](snake.html) snake

[status.cgi](status.cgi) core logig of the k4cg mqtt to json converter

[tron.html](tron.html) lightcycle game

[BraynFxxxToGo.wdgt](BraynF***ToGo.wdgt) MacOS dashboard widget for running BrainFuck

[gengif](gengif) generate high quality gifs using ffmpeg
