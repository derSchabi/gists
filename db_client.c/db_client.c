#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "dickbut.h"

#define PORT_TIME       13              /* "time" (not available on RedHat) */
#define SERVER_ADDR     "151.217.47.77"     /* localhost */
#define MAXBUF          1024
#define PORT	        8080

int x = 0;
int y = 0;
int t = 0;

int r = 219;
int g = 112;
int b = 147;

void send_pix(int sockfd, char buffer[])
{
	int i, j;	
	for(i = 0; i < gimp_image.width; i++) {
		for(j = 0; j < gimp_image.height; j++) {
			//printf("%02x", gimp_image.pixel_data[j*gimp_image.width*3 + i*3]);
			if(gimp_image.pixel_data[j*gimp_image.width*3 + i*3] < 0x10) {
				//printf("%02x\n", gimp_image.pixel_data[j*gimp_image.width*3 + i*3]);
				/*sprintf(buffer, "PX %d %d %02x%02x%02x\n", i+x, j+y, 
					gimp_image.pixel_data[j*gimp_image.width*3 + i*3],
					gimp_image.pixel_data[j*gimp_image.width*3 + i*3 +1],
					gimp_image.pixel_data[j*gimp_image.width*3 + i*3 +2]);*/
				sprintf(buffer, "PX %d %d %02x%02x%02x\n", i+x, j+y, r, g, b);
					
				write(sockfd, buffer, strlen(buffer));
			}

		}

	}		
}

int main()
{   int sockfd;
    struct sockaddr_in dest;
    char buffer[MAXBUF];

    t = time(NULL);
    srand(t);

    /*---Open socket for streaming---*/
    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
    {
        perror("Socket");
        exit(errno);
    }

    /*---Initialize server address/port struct---*/
    bzero(&dest, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(PORT);
    if ( inet_aton(SERVER_ADDR, &dest.sin_addr.s_addr) == 0 )
    {
        perror(SERVER_ADDR);
        exit(errno);
    }

    /*---Connect to server---*/
    if ( connect(sockfd, (struct sockaddr*)&dest, sizeof(dest)) != 0 )
    {
        perror("Connect ");
        exit(errno);
    }

    for(;;) {
    	send_pix(sockfd, buffer);
        if(time(NULL) > t + 5) {
		t = time(NULL);
		x = rand() % 500;
		y = rand() % 500;

        r = rand() % 255;
        g = rand() % 255;
        b = rand() % 255;
	}
    }


    /*---Clean up---*/
    close(sockfd);
    return 0;
}


