#include <Ethernet.h>
#include <SPI.h>

//pixelflut display
IPAddress server(151,217,47,77);
const int port = 8080;

// display offset
const int x = 550;
const int y = 10;

//rgb hex color string
#define color "00ff00"

byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};

EthernetClient client;

void setup() {
  Serial.begin(9600);
  
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for (;;);
  }
  
  // print ip
  Serial.print("Ip addr: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("connectin ...");
  if(client.connect(server, port)) {
    Serial.println("connected");
  } else {
    Serial.println("connection failed");
    for(;;);
  }
}

void write_lpx(int x, int y) {
  client.print("PX ");
  client.print(String(x));
  client.print(" ");
  client.print(String(y));
  client.print(" ");
  client.println(color);
}

void send_pix()
{
  for(int i = 0; i < 20; i++) {
    for(int j = 0; j < 20; j++) {
         write_lpx(x+i, y+j);
    }
  }
}

void loop() {
  send_pix();
}

