#!/bin/bash

NAME=$1
IP=$2

commands() {
    echo "n $NAME"
    I=0
    while true; do
	I=$(($I + 1))
	echo $(($RANDOM % 360)).$(($RANDOM % 100))
	if [ 0 -eq $(($I % 10)) ]; then
	    echo "c"
	fi
	sleep 2
    done
}

commands | nc $IP 3490 > /dev/null